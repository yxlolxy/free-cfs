package env

import "os"

var (
	QiniuAccessKey      = os.Getenv("QINIU_ACCESS_KEY")
	QiniuSecretKey      = os.Getenv("QINIU_SECRET_KEY")
	QiniuBucket         = os.Getenv("QINIU_BUCKET")
	QiniuDownloadDomain = os.Getenv("QINIU_DOWNLOAD_DOMAIN") // 七牛云测试CDN域名
	QiniuUploadDomain   = os.Getenv("QINIU_UPLOAD_DOMAIN")   // https://developer.qiniu.com/kodo/1671/region-endpoint-fq
	StockAiAppVersion   = os.Getenv("STOCK_AI_APP_VERSION")
	StockAiUpgradeUrl   = os.Getenv("STOCK_AI_UPGRADE_URL")
	StockAiServer       = os.Getenv("STOCK_AI_SERVER")
	GPTTOKEN          = os.Getenv("GPT_TOKEN")
)
