package main

import (
	"free-cfs/utils"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func handler(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	method := request.HTTPMethod
	switch method {
	case "GET":
		return handleGet(request)
	default:
		return utils.ErrorResponse("请求方法不支持")
	}
}

func handleGet(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	path := request.Path
	pathParameter := strings.Split(path, "/tools")[1]
	switch pathParameter {
	case "/isTodayTradeDay":
		return utils.SuccessResponse(utils.IsTodayTradeDay())
	default:
		return utils.ErrorResponse("请求路径不存在")
	}
}

func main() {
	lambda.Start(handler)
}
