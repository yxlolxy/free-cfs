package main

import (
	"context"
	"encoding/json"
	"free-cfs/env"
	"free-cfs/types"
	"free-cfs/utils"

	"github.com/aws/aws-lambda-go/events"
	openai "github.com/yxlolxy/go-gpt"
)

func GPT351(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	gpttoken := request.Headers["gpttoken"]
	if gpttoken != env.GPTTOKEN  {
		return utils.ErrorResponse("权限检验失败")
	}
	var body types.GptBody
	json.Unmarshal([]byte(request.Body), &body)
	if (body.Token == "" || body.Message == "" || body.Url == "") {
		return utils.ErrorResponse("参数缺失")
	}
	openai.SetOpenAiUrlV1(body.Url + "/v1")
	client := openai.NewClient(body.Token)

	resp, _ := client.CreateChatCompletion(
		context.Background(),
		openai.ChatCompletionRequest{
			Model: openai.GPT3Dot5Turbo,
			Messages: []openai.ChatCompletionMessage{
				{
					Role:    openai.ChatMessageRoleUser,
					Content: body.Message,
				},
			},
		},
	)
	return utils.SuccessResponse(resp.Choices[0].Message.Content)
}
