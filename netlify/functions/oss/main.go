package main

import (
	"free-cfs/utils"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func handler(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	method := request.HTTPMethod
	switch method {
	case "GET":
		return handleGet(request)
	case "POST":
		return handlePost(request)
	case "PUT":
		return handlePut(request)
	case "DELETE":
		return handleDelete(request)
	default:
		return utils.ErrorResponse("请求方法不支持")
	}
}

func handleGet(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	path := request.Path
	pathParameter := strings.Split(path, "/oss")[1]
	switch pathParameter {
	case "/uploadToken":
		return GetUploadToken(request)
	case "/downloadUrl":
		return GetDownloadUrl(request)
	default:
		return utils.ErrorResponse("请求路径不存在")
	}
}
func handlePost(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	return utils.ErrorResponse("请求方法不支持")
}
func handlePut(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	return utils.ErrorResponse("请求方法不支持")
}
func handleDelete(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	return utils.ErrorResponse("请求方法不支持")
}

func main() {
	lambda.Start(handler)
}
