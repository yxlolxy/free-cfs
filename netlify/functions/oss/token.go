package main

import (
	"fmt"
	"free-cfs/env"
	"free-cfs/utils"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/qiniu/go-sdk/v7/auth"
	"github.com/qiniu/go-sdk/v7/storage"
)

func GetUploadToken(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	token := request.Headers["stockaitoken"]
	if token == "" {
		return utils.ErrorResponse("权限检验失败")
	}
	accessKey := env.QiniuAccessKey
	secretKey := env.QiniuSecretKey
	bucket := env.QiniuBucket
	if accessKey == "" || secretKey == "" || bucket == "" {
		return utils.ErrorResponse("OSS配置错误，请联系管理员")
	}
	token, err := utils.Base64Decrypt(token)
	if err != nil || token == "" {
		return utils.ErrorResponse("令牌解析错误，请联系管理员")
	}
	arr := strings.Split(token, "@")
	if len(arr) != 2 || arr[0] == "" {
		return utils.ErrorResponse("令牌解析错误，请联系管理员")
	}
	filename := arr[0] + ".db"
	putPolicy := storage.PutPolicy{
		Scope: fmt.Sprintf("%s:%s", bucket, filename),
	}
	mac := auth.New(accessKey, secretKey)
	upToken := putPolicy.UploadToken(mac)

	return utils.SuccessResponse(upToken)
}

func GetDownloadUrl(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	token := request.Headers["stockaitoken"]
	if token == "" {
		return utils.ErrorResponse("权限检验失败")
	}
	accessKey := env.QiniuAccessKey
	secretKey := env.QiniuSecretKey
	bucket := env.QiniuBucket
	if accessKey == "" || secretKey == "" || bucket == "" {
		return utils.ErrorResponse("OSS配置错误，请联系管理员")
	}
	token, err := utils.Base64Decrypt(token)
	if err != nil || token == "" {
		return utils.ErrorResponse("令牌解析错误，请联系管理员")
	}
	arr := strings.Split(token, "@")
	if len(arr) != 2 || arr[0] == "" {
		return utils.ErrorResponse("令牌解析错误，请联系管理员")
	}
	filename := arr[0] + ".db"
	downloadUrl := "http://" + env.QiniuDownloadDomain + "/" + filename + "?e=" + strconv.FormatInt(time.Now().Add(600e9).Unix(), 10)
	token = accessKey + ":" + utils.HmacSha1(downloadUrl, secretKey)
	downloadUrl = downloadUrl + "&token=" + token
	return utils.SuccessResponse(downloadUrl)
}
