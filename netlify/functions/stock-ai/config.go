package main

import (
	"free-cfs/env"
	"free-cfs/types"
	"free-cfs/utils"

	"github.com/aws/aws-lambda-go/events"
)

func GetConfig(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	return utils.SuccessResponse(
		types.StockAiConfigType{
			RemoteServer:        env.StockAiServer,
			QiniuDownloadDomain: env.QiniuDownloadDomain,
			QiniuUploadDomain:   env.QiniuUploadDomain,
			StockAiAppVersion:   env.StockAiAppVersion,
			StockAiUpgradeUrl:   env.StockAiUpgradeUrl,
		},
	)
}
