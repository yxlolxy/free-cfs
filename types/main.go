package types

type Response struct {
	Data  any `json:"data"`
	Error any `json:"error"`
}

type StockAiConfigType struct {
	RemoteServer        string `json:"remoteServer"`
	QiniuDownloadDomain string `json:"ossServer"`
	QiniuUploadDomain   string `json:"ossRegion"`
	StockAiAppVersion   string `json:"appVersion"`
	StockAiUpgradeUrl   string `json:"appDownload"`
}

type GptBody struct {
	Message string `json:"message"`
	Token   string `json:"token"`
	Url     string `json:"url"`
}