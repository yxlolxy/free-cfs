module free-cfs

go 1.18

require (
	github.com/aws/aws-lambda-go v1.41.0
	github.com/qiniu/go-sdk/v7 v7.15.0
	github.com/yxlolxy/go-gpt v1.0.0
)

require golang.org/x/sync v0.0.0-20220722155255-886fb9371eb4 // indirect
