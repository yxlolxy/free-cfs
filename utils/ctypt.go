package utils

import (
	"crypto/hmac"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"strings"
)

var (
	priPem = `-----BEGIN RSA PRIVATE KEY-----
	MIICWwIBAAKBgQDIZR0Mlb+jKCVaC+yVLAvzaeje7EYbN9z28lsoLXZ8meh7TUmT
	h0XwB2+EMTXbds7oJMlOXfl03Xc0xuB+qW4DgQVdla7BUw7giu8AD9B318TSoLfE
	VGO1iaW+iO98AAkdv+UQ8elHjYlK2Cn5ea8pC9pKmRcww1h8/gsvFd+Z+wIDAQAB
	AoGAe7wDTcwBAis9tPX75LfHwUq54Tuephu+MjS/GHXJ3XHi3I+CSLeT9XuB3evM
	x2/6ndho/ByGdSRyAIJ0B4FyTVoh9nMh781IyRczndh5ZmV+9fCi4RNJkQWrlxpT
	OAxrhA2DRP9Uw14jU7RcungIpFns/Hp98pCzJZbpcuSPyOECQQDKo7gFZ2R0gnpV
	FIPsNpcTzfJg4f+LIsbVLwgG+JsalmM6PJ6JEQCtRnHQxMtL6w6VxsQxTMGL/VJB
	mruJvX9TAkEA/SoV4uxPC4MVpr+D/unPxHMAY7ZuFulF9kqTZbJJUgkWzbtCEqTq
	YTzADfwINBkTZcI0R92VcsMK/YfCOJ8tuQJAEAgt6feJIhKknRCXc1vKTh3QHEwl
	DJv+wSUMbIjEtLDLPvNG069Nr2fQX2UCdIovSDhGk2PwOCQdmuUkCzy70wJAFN4V
	M8E6ur4xWK9MmAR5xb2c+Jse8/CCfcyj3awWcxagtJCM37CMSTuSsRRnMLQobyjU
	2l+fY/hDvaYlEjm8sQJAacK53TcVIMfR7kFpjJh+xOkaS5R76WiOgmBJO58V+Zh2
	RVObZTbmXC5JpmxVlTeuGnjY8LmG3Pv4TqfAKw4PYw==
	-----END RSA PRIVATE KEY-----`
	pubPem = `-----BEGIN PUBLIC KEY-----
	MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDIZR0Mlb+jKCVaC+yVLAvzaeje
	7EYbN9z28lsoLXZ8meh7TUmTh0XwB2+EMTXbds7oJMlOXfl03Xc0xuB+qW4DgQVd
	la7BUw7giu8AD9B318TSoLfEVGO1iaW+iO98AAkdv+UQ8elHjYlK2Cn5ea8pC9pK
	mRcww1h8/gsvFd+Z+wIDAQAB
	-----END PUBLIC KEY-----`
)

func GetBase64RsaPubPem() string {
	return base64.StdEncoding.EncodeToString([]byte(pubPem))
}

func DecryptRsa(input string) (string, error) {
	priBlock, _ := pem.Decode([]byte(strings.ReplaceAll(priPem, "\t", "")))
	priKey, err := x509.ParsePKCS1PrivateKey(priBlock.Bytes)
	if err != nil {
		return "", err
	}
	decryptedBytes, err := rsa.DecryptPKCS1v15(rand.Reader, priKey, []byte(input))
	if err != nil {
		return "", err
	}
	return string(decryptedBytes), nil
}

func Base64Decrypt(input string) (string, error) {
	bytes, err := base64.StdEncoding.DecodeString(input)
	return string(bytes), err
}

func HmacSha1(value string, key string) string {
	keyStr := []byte(key)
	mac := hmac.New(sha1.New, keyStr)
	mac.Write([]byte(value))
	return base64.StdEncoding.EncodeToString(mac.Sum(nil))
}
