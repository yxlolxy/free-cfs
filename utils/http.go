package utils

import (
	"encoding/json"
	"free-cfs/types"
	"net/http"

	"github.com/aws/aws-lambda-go/events"
)

func ErrorResponse(err string) (*events.APIGatewayProxyResponse, error) {
	bytes, _ := json.Marshal(types.Response{
		Error: err,
	})
	return &events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Body:       string(bytes),
	}, nil
}

func SuccessResponse(data any) (*events.APIGatewayProxyResponse, error) {
	bytes, _ := json.Marshal(types.Response{
		Data: data,
	})
	return &events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Body:       string(bytes),
	}, nil
}
