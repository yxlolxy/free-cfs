package utils

import (
	"encoding/json"
	"io"
	"net/http"
	"time"
)

func formatDateAndTime(formatter string, t any) string {
	if t == nil {
		t = time.Now()
	}
	if val, ok := t.(time.Time); ok {
		return val.Format(formatter)
	} else {
		panic("日期时间转换错误")
	}
}

func FormatDateTime(t any) string {
	return formatDateAndTime("2006-01-02 15:04:05", t)
}

func FormatDate(t any) string {
	return formatDateAndTime("2006-01-02", t)
}

func ParseTime(value string) time.Time {
	loc, err := time.LoadLocation("Asia/Shanghai")
	if err != nil {
		panic(err)
	}

	t, err := time.ParseInLocation("2006-01-02 15:04:05", value, loc)
	if err != nil {
		panic(err)
	}
	return t
}

func ParseDate(value string) time.Time {
	loc, err := time.LoadLocation("Asia/Shanghai")
	if err != nil {
		panic(err)
	}

	t, err := time.ParseInLocation("2006-01-02", value, loc)
	if err != nil {
		panic(err)
	}
	return t
}

// func IsTodayTradeDay() bool {
// 	// 0-工作日, 1-休息日, 2-节假日
// 	resp, err := http.Get("http://tool.bitefu.net/jiari/?d=" + FormatDate(nil))

// 	if err != nil {
// 		panic(err)
// 	}
// 	defer resp.Body.Close()
// 	bytes, err := io.ReadAll(resp.Body)
// 	if err != nil {
// 		panic(err)
// 	}
// 	var result int
// 	err = json.Unmarshal(bytes, &result)
// 	if err != nil {
// 		panic(err)
// 	}
// 	var tw = time.Now().Weekday()
// 	return tw >= 1 && tw <= 5 && result == 0
// }

type SzseDataType struct {
	Zrxh int    `json: "zrxh"` // 1代表星期日 2 星期一 7代表星期6
	Jybz string `json: "jybz"` // 1 是交易日 0不是
	Jyrq string `json: "jyrq"` // 交易日期格式2023-05-01

}
type SzseResDataType struct {
	Data    []SzseDataType
	Nowdate string `json: "nowdate"`
}

var szseResData SzseResDataType

// 通过深证交易日历获取 http://www.szse.cn/aboutus/calendar/index.html
func IsTodayTradeDay() bool {
	if szseResData.Nowdate != "" && ParseDate(szseResData.Nowdate).Month() == time.Now().Month() {
		res := szseResData
		var target SzseDataType
		result := Find(res.Data, &target, func(item SzseDataType) bool {
			return item.Jyrq == FormatDate(nil)
		})
		if result {
			szseResData = res
			return target.Jybz == "1"
		}
		return false
	}
	resp, err := http.Get("http://www.szse.cn/api/report/exchange/onepersistenthour/monthList")
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	var res SzseResDataType
	bytes, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(bytes, &res)
	if err != nil {
		panic(err)
	}
	var target SzseDataType
	result := Find(res.Data, &target, func(item SzseDataType) bool {
		return item.Jyrq == FormatDate(nil)
	})
	if result {
		szseResData = res
		return target.Jybz == "1"
	}
	return false
}
