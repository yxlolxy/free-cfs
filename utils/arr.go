package utils

func Find[T any](arr []T, result *T, where func(T) bool) bool {
	if arr == nil {
		return false
	}
	var res bool = false
	for _, val := range arr {
		if where(val) {
			res = true
			*result = val
			break
		}
	}
	return res
}
